"use strict";

let mainBox = document.querySelector(".container");
let box = document.querySelector(".container__input");
let inputArea = document.querySelector(".price--input");
let form = document.querySelector(".form--input");
let header = document.querySelector(".header--input");
let containerBox, btnCross1, btnCross2, btn, errorMsg;
let price = 0;

window.onload = setTimeout(() => {
  box.classList.remove("display--off");
  box.classList.add("animation--box");
}, 1500);

inputArea.addEventListener("blur", () => {
  box.insertAdjacentHTML(
    "beforebegin",
    `<span class="box__value container__input header--input">Current value: $ ${(price = Number(
      inputArea.value.replace(/[^\d.-]/g, "")
    ))}<span class="btn"><span class="btn--cross"></span><span class="btn--cross--cross"></span></span>`
  );
  containerBox = document.querySelector(".box__value");
  btnHover();
  focusOff();
});

inputArea.addEventListener("focus", () => {
  if (inputArea != null) {
    focusOn();
  }
});

/* --------------------------------- FUNCTIOnS ---------------------------------------- */

function focusOff() {
  mainBox.classList.remove("container--focus");

  box.classList.remove("container__input--focus");

  header = document.querySelector(".header--input");
  header.classList.add("animate__fadeInUp", "animation--box");

  if (price < 0 && inputArea != null) {
    form.insertAdjacentHTML(
      "beforeend",
      `<span class="error__message">Please enter correct price</span>`
    );
    inputArea.classList.add("error--area", "price--input--unfocus");
    inputArea.classList.remove("price--input", "price--input--focus");
    errorMsg = document.querySelector(".error__message");
  }
}

function focusOn() {
  mainBox.classList.add("container--focus");

  header.classList.add("animation--box");

  box.classList.add("container__input--focus");

  header.remove();
  header = document.querySelector(".header--input");

  if (price > 0 && inputArea != null) {
    inputArea.classList.remove("error--area", "price--input--unfocus");
    inputArea.classList.add("price--input", "price--input--focus");
    errorMsg.remove();
  }
}

function btnHover() {
  btn = document.querySelector(".btn");
  btnCross1 = document.querySelector(".btn--cross");
  btnCross2 = document.querySelector(".btn--cross--cross");
  btn.addEventListener("mouseover", () => {
    btnCross1.classList.add("btn--hover");
    btnCross2.classList.add("btn--hover");
  });
  btn.addEventListener("mouseout", () => {
    btnCross1.classList.remove("btn--hover");
    btnCross2.classList.remove("btn--hover");
  });
  btn.addEventListener("click", (e) => {
    containerBox.classList.add("box__value--del");
    containerBox.remove();
  });
}
