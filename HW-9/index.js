"use strict";

document.querySelector(".tabs").addEventListener("click", (e) => {
  document.querySelector(".tabs-title.active").classList.remove("active");
  e.target.classList.add("active");

  document
    .querySelector(".content-item.display-on")
    .classList.add("display-off");

  document
    .querySelector(".content-item.display-on")
    .classList.remove("display-on");

  let text = document.querySelector(
    '.content-item[data-name="' + e.target.dataset.tabsTitle + '"]'
  );

  if (e.target.dataset.tabsTitle == text.dataset.name) {
    text.classList.remove("display-off");
    text.classList.add("display-on");
  }
});
