"use strict";

$(".arrow-up").css("display", "none");
$(document).scroll(() => {
  var y = $(this).scrollTop();
  if (y >= 300) {
    $(".arrow-up").fadeIn();
  } else {
    $(".arrow-up").fadeOut();
  }
});

$(".top__rated").css("display", "none");
$(".slide-toggle").click(() => {
  $(".top__rated").slideToggle();
});
