"use strict";

//alert("connected");

let list = document.querySelector(".list");
let timer = document.querySelector(".timer");
let arr = ["hello", ["world", "Kiev"], "Kharkiv", "Odessa", "Lviv"];

function createList(arr, parent = document.body) {
  let ol = document.createElement("ol");
  const listLi = arr.map((item) => {
    if (Array.isArray(item) == true) {
      createList(item, ol);
    } else {
      return `<li>${item}</li>`;
    }
  });
  ol.insertAdjacentHTML("afterbegin", listLi.join(""));
  parent.append(ol);
}

//createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], list);
createList(arr, list);

setInterval(() => {
  +timer.innerHTML > 0
    ? (timer.innerHTML -= 1)
    : (document.body.innerHTML = "<h2>The list was deleted</h2>");
}, 1000);
