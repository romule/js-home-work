"use strict";

let eyes = document.querySelectorAll(".input-wrapper"),
  inputArea = document.querySelectorAll("input"),
  form = document.querySelector("form");

for (let i = 0; i < eyes.length; i++) {
  eyes[i].addEventListener("click", (e) => {
    if (e.target.classList.contains("fa-eye")) {
      e.target.classList.toggle("fa-eye");
      e.target.classList.add("fa-eye-slash");
      inputArea[i].type = "password";
    } else {
      e.target.classList.add("fa-eye");
      e.target.classList.toggle("fa-eye-slash");
      inputArea[i].type = "text";
    }
  });
}

function check(pass, confPass) {
  if (pass == confPass && (pass != "" || confPass != "")) return true;
  else return false;
}

function errorMsg() {
  if (document.querySelector("span")) return;
  else {
    document
      .querySelector(".btn")
      .insertAdjacentHTML(
        "beforebegin",
        '<span style="color: #f00">Нужно ввести одинаковые значения</span>'
      );
  }
}

form.addEventListener("submit", () => {
  if (check(inputArea[0].value, inputArea[1].value))
    document.querySelector(".modal").style.display = "block";
  else errorMsg();
});
