"use strict";

const btn = document.querySelector(".btn-theme");

function setTheme() {
  for (
    let i = 0;
    i <
    document.querySelectorAll(
      "body, .logo, .menu, .nav__aside-item, aside, section, main, footer"
    ).length;
    i++
  ) {
    document
      .querySelectorAll(
        "body, .logo, .menu, .nav__aside-item, aside, section, main, footer"
      )
      [i].classList.toggle("change-theme");
  }
}

document.addEventListener("DOMContentLoaded", () => {
  if (localStorage.getItem("darkTheme") == "on") setTheme();
  else return;
});

btn.addEventListener("click", (e) => {
  setTheme();
  if (document.body.classList.contains("change-theme"))
    localStorage.setItem("darkTheme", "on");
  else localStorage.setItem("darkTheme", "off");
});
