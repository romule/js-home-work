"use strict";

function filterBy(array, dataType) {
  let newArr = [];
  array.forEach(function (item) {
    if (typeof item != dataType) newArr.push(item);
  });
  return newArr;
}

console.log(filterBy(["hello", "world", 23, "23", null], "string"));
