"use strict";

let num;

do {
  num = Number(prompt("Give me a number: ", num));
} while (num % 1 != 0 && num);

for (let i = 0; i <= num; i++) {
  if (i % 5 == 0) {
    console.log(i);
  } else if (num < 5) {
    alert("Sorry no numbers");
    break;
  }
}
