"use strict";

const body = document.querySelector("body"),
  btn = document.querySelectorAll(".btn");

body.addEventListener("keydown", (e) => {
  for (let i = 0; i < btn.length; i++) {
    if (btn[i].innerText.toUpperCase() == e.key.toUpperCase())
      btn[i].classList.add("btn-cklicked");
    else btn[i].classList.remove("btn-cklicked");
  }
});
