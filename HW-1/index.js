"use strict";

let name = prompt("Please type your name: ", "exp.Sem,Mark");
let age = prompt("Your age: ", "exp.: 25, 18");

if (isNaN(age)) {
  age = prompt("The age is incorrect: ", `${age}`);
} else if (!name.match(/^[a-zA-Z]+$/)) {
  name = prompt("The name is incorrect: ", `${name}`);
}

// age = Number(age);

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  let conf = confirm("Are you sure you want to continue?");
  if (conf == true) {
    alert(`Welcome, ${name}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${name}`);
}
